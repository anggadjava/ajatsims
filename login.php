<?php include("include/connect.php"); ?>
<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
<title><?php echo ucwords($rstitle)?></title>

    
    <link rel="stylesheet" href="css/login/css/reset.css">

    
        <link rel="stylesheet" href="css/login/css/style.css">
<SCRIPT>
function jumpTo (link){
   var new_url=link;
   if ((new_url != "")  &&  (new_url != null))
   window.location=new_url;
}

jQuery(document).ready(function(){
  jQuery("#NIP").keyup(function(event){
    if(event.keyCode == 13){
      MyAjaxRequest('valid_nip','include/process.php?NIP=','NIP');
      jQuery("#PWD").focus();
    }
  });

  jQuery("#PWD").keyup(function(event){
    if(event.keyCode == 13){
      //MyAjaxRequest('valid_pwd','include/process.php?PWD=','PWD');
      jQuery('#frm').submit();
    }
  });
});
</script>
    
    
    
  </head>

  <body>
<form name="frm" id="frm" action="user_level.php" method="post">
<?php

  if(isset($_POST['signin'])){
    require_once("login.php");
  }
?>
    <div class="wrap">
    <div class="avatar">
      <img src="img/logo_unhas.jpg">
    </div>
    <input placeholder="username" class="text" id="NIP" type="text" size="25" name="USERNAME" onBlur="javascript: MyAjaxRequest('valid_nip','include/process.php?NIP=','NIP');return false;" /><span id="valid_nip"></span>
    <div class="bar">
      <i></i>
    </div>
    <input placeholder="********" class="text" type="password" id="PWD" size="25" name="PWD" /><span id="valid_pwd"></span>
    <a href="" class="tooltips forgot_link">Forgot ?<span>Mohon Contact SIM</span></a>
    <button onclick="document.getElementById('frm').submit();">Sign in</button>
  </div>

</form>
    
    
  </body>
  <script type="text/javascript" language="javascript" src="include/ajaxrequest.js"></script>
<script src="js/jquery-1.7.min.js" language="JavaScript" type="text/javascript"></script>    
</html>
