taroh ini di procedure pr_savebill_tindakanrajal_dokter

```
#!sql

BEGIN
DECLARE iNOBILL INT;
DECLARE iJasaSarana INT;
DECLARE iJasaPelayanan INT;
DECLARE iTARIFRS INT;
DECLARE iNOBAYAR INT;
DECLARE iTGLBAYAR DATE;
DECLARE iJAMBAYAR TIME;
DECLARE iJMBAYAR INT;
DECLARE iNIP VARCHAR(50);
DECLARE iSHIFT SMALLINT;
DECLARE iTBP SMALLINT;
DECLARE iLUNAS SMALLINT;
DECLARE iSTATUS VARCHAR(15);

SELECT nomor FROM m_maxnobill INTO iNOBILL;
IF iNOBILL IS NULL THEN
  SET iNOBILL=0;
ELSE
  
  SET iNOBILL=iNOBILL+1;
END IF;

UPDATE m_maxnobill SET NOMOR=iNOBILL;
INSERT INTO t_billrajal(KODETARIF, NOMR, TANGGAL, SHIFT, NIP, QTY, IDXDAFTAR, NOBILL, JASA_SARANA,JASA_PELAYANAN,TARIFRS,KDPOLY,CARABAYAR,APS,KDDOKTER,UNIT)
   SELECT a.KODETARIF, inNOMR, inTANGGAL, inSHIFT, inNIP, a.QTY, inIDXDAFTAR, iNOBILL, b.jasa_sarana,b.jasa_pelayanan,b.tarif,inPoly,inCARABAYAR,inAPS,inDokter,inUNIT
   FROM tmp_cartbayar a, m_tarif2012 b WHERE a.KODETARIF=b.kode_tindakan AND a.IP=inIPnya;
SELECT NOMOR FROM m_maxnobyr INTO iNOBAYAR;
IF iNOBAYAR IS NULL THEN
  SET iNOBAYAR =1;
ELSE
  SET iNOBAYAR = iNOBAYAR +1;
END IF;
UPDATE m_maxnobyr SET NOMOR=iNOBAYAR;
SELECT SUM(JASA_SARANA*QTY) AS iJasaSarana, SUM(JASA_PELAYANAN*QTY) AS iJasaPelayanan, SUM(TARIFRS*QTY) AS iTARIFRS
INTO iJasaSarana,iJasaPelayanan,iTARIFRS FROM t_billrajal WHERE NOBILL= iNOBILL AND idxdaftar=inIDXDAFTAR;
IF inCARABAYAR > 1 THEN
  SET iTGLBAYAR = CURDATE();
  SET iJAMBAYAR = CURTIME();
  SET iJMBAYAR  = iTARIFRS;
  SET iNIP	= inNIP;
  SET iSHIFT 	= inSHIFT;
  SET iTBP	= 0;
  SET iLUNAS	= 1;
  SET iSTATUS	= 'LUNAS';
ELSE
  SET iTGLBAYAR = '';
  SET iJAMBAYAR = '';
  SET iJMBAYAR  = '';
  SET iNIP	= '';
  SET iSHIFT 	= '';
  SET iTBP	= '';
  SET iLUNAS	= '';
  SET iSTATUS	= 'TRX';
END IF;
INSERT INTO t_bayarrajal(NOMR,SHIFT,NIP,IDXDAFTAR,NOBILL, TOTJASA_SARANA,TOTJASA_PELAYANAN,TOTTARIFRS,CARABAYAR,APS,TGLBAYAR,JAMBAYAR,JMBAYAR,TBP,LUNAS,STATUS,UNIT)
VALUES(inNOMR, inSHIFT, inNIP, inIDXDAFTAR, iNOBILL, iJasaSarana,iJasaPelayanan,iTARIFRS,inCARABAYAR,inAPS,iTGLBAYAR,iJAMBAYAR,iJMBAYAR,iTBP,iLUNAS,iSTATUS,inUNIT);
DELETE FROM tmp_cartbayar WHERE IP=inIPnya;
    END

```