<?php 

$tgl_kunjungan = "";
if(!empty($_GET['tahun'])){
	
	$tahun	= isset($_REQUEST['tahun']) ? $_REQUEST['tahun'] : date('Y');
	$bulan	= isset($_REQUEST['bulan']) ? $_REQUEST['bulan'] : date('m');
}else{
	$tahun	= date('Y');
	$bulan	= date('m');
}

?>


<div align="center">
    <div id="frame" style="width:95%">
    <div id="frame_title">
			
		<table cellpadding="0" class="tb" width="95%" cellspacing="0">
			<tr><td rowspan="2" style="width:110px;"><img src="<?php echo _BASE_;?>/img/logobaktihusda.gif"></td><td><h2>Formulir 5.2</h2></td><td rowspan="2"><div style="border:1px dashed #999; padding:10px; display:block; font-style:italic; width:170px;">Ditjen Bina Upaya Kesehatan <br />Kementrian Kesehatan RI</div> 
</td></tr>
			<tr><td><h1>Daftar 10 Besar Penyakit Rawat Inap</h1></td></tr>
		</table>
			
			<br><br>
			<table cellpadding="0" class="tb" width="95%" cellspacing="0">
				<tr><td> Kode RS </td><td>: <?php echo $kode_rs;?></td></tr>
                <tr><td> Nama RS </td><td>: <?php echo $nama_rs;?></td></tr>
                <tr><td> Tahun </td><td>: <?php echo $tahun;?></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
			</table>
			
			<table cellspacing="1" cellpadding="1" class="tb" width="95%">
			<thead>
				<tr><th rowspan="2">NO URUT</th><th rowspan="2">ICD</th><th rowspan="2">DESKRIPSI</th><th colspan="2">Pasien Keluar (Hidup & Mati) Menurut Jenis Kelamin</th><th rowspan="2">Jumlah Pasien Keluar Hidup</th><th rowspan="2">Jumlah Pasien Keluar Mati</th></tr>
				<tr><th>LK</th><th>PR</th></tr>
				<tr><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td></tr>
			</thead>
			<tbody>
				<?php
				$sql = mysql_query("SELECT a.NOMR,a.ICD_CODE,b.jenis_penyakit, COUNT(a.ICD_CODE) AS jumlah, SUM(IF(c.JENISKELAMIN = 'P',1,0)) AS pr, SUM(IF(c.JENISKELAMIN = 'L',1,0)) AS lk
,SUM(IF(KDTUJUANRUJUK <> 2 OR KDTUJUANRUJUK <> 6, 1, 0)) AS hidup, SUM(IF(KDTUJUANRUJUK = 2 OR KDTUJUANRUJUK = 6, 1, 0)) AS mati
FROM t_diagnosadanterapi a
JOIN icd b ON a.icd_code=b.icd_code 
JOIN m_pasien c ON a.NOMR = c.NOMR
WHERE YEAR(a.TANGGAL) = ".$tahun."
GROUP BY a.ICD_CODE ORDER BY jumlah DESC LIMIT 10");
				if(mysql_num_rows($sql) > 0)
				{
					while($data = mysql_fetch_array($sql)){
						echo '<tr><td>1</td><td>'.$data['ICD_CODE'].'</td><td>'.$data['jenis_penyakit'].'</td><td>'.$data['lk'].'</td><td>'.$data['pr'].'</td><td>'.$data['hidup'].'</td><td>'.$data['mati'].'</td></tr>';
					}
				}
				?>
			</tbody>
			</table>
    </div>
	</div>
</div>
