<?php 

$tgl_kunjungan = "";
if(!empty($_GET['tahun'])){
	
	$tahun	= isset($_REQUEST['tahun']) ? $_REQUEST['tahun'] : date('Y');
}else{
	$tahun	= date('Y');
}
$sql_showing = 'SELECT a.nomor_kode_rs, DATE_FORMAT(a.tanggal_registrasi,"%d/%m/%Y") AS tanggal_registrasi, a.nama_rumah_sakit,  CONCAT(b.jenis," ",b.uraian) AS jenis_rumah_sakit, CONCAT(c.kepemilikan," ",c.kelas) AS kelas_rumah_sakit, a.nama_direktur_rs, nama_penyelenggara_rs, alamat_lokasi_rs, a.kab_kota, a.kode_pos, a.telepon, a.fax, a.email, a.nomor_telp_bag_umum, a.website, a.tanah, a.bangunan, a.nomor, DATE_FORMAT(a.tanggal,"%d/%m/%Y") AS tanggal, a.oleh, a.sifat, a.masa_berlaku_thn, d.nama AS status_penyelenggara_swasta, a.akreditasi_rs, e.pentahapan, a.status, DATE_FORMAT(a.tanggal_akreditasi,"%d/%m/%Y") AS tanggal_akreditasi, a.vvip, a.vip,a.i, a.ii, a.iii, a.dokter_spa, a.dokter_spog, a.dokter_sppd, a.dokter_spb, a.dokter_sprad, a.dokter_sprm, a.dokter_span, a.dokter_spjp, a.dokter_spm, a.dokter_sptht, a.dokter_spkj, a.dokter_umum, a.dokter_gigi, a.dokter_gigi_spesialis, a.perawat, a.bidan, a.farmasi, a.tenaga_kesehatan_lainnya, a.tenaga_non_kesehatan, a.tahun 
FROM data_dasar a 
LEFT JOIN jenis_rumahsakit b ON a.jenis_rumah_sakit = b.id
LEFT JOIN kelas c ON a.kelas_rumah_sakit = c.id
LEFT JOIN penyelenggara_swasta d ON a.status_penyelenggara_swasta = d.id
LEFT JOIN pentahapan_akreditasi e ON a.pentahapan = e.id
LEFT JOIN status_akreditasi f ON a.status = f.id
WHERE a.tahun = "'.$tahun.'"';
$sql_show	 = mysql_query($sql_showing);
$data		 = mysql_fetch_array($sql_show);
?>


<div align="center">
    <div id="frame" style="width:95%">
    <div id="frame_title">
    <table border="0" width="95%">
		<tr valign="top">
			<td align="center">
			
				<table cellpadding="0" class="tb" width="95%" cellspacing="0">
					<tr><td rowspan="2" style="width:110px;"><img src="<?php echo _BASE_;?>/img/logobaktihusda.gif"></td><td><h2>Formulir 2</h2></td><td rowspan="2"><div style="border:1px dashed #999; padding:10px; display:block; font-style:italic; width:170px;">Ditjen Bina Upaya Kesehatan <br />Kementrian Kesehatan RI</div> 
	</td></tr>
					<tr><td><h1>DATA DASAR RUMAH SAKIT</h1></td></tr>
				</table>
				<br><br>
				<table cellpadding="0" class="tb" width="95%" cellspacing="0">
					<tr><td> Kode RS </td><td colspan="2">: </td></tr>
					<tr><td> Nama RS </td><td colspan="2">: </td></tr>
					<tr><td> Tahun </td><td colspan="2">: <?php echo $tahun;?></td></tr>
					<tr><td colspan="3">&nbsp;</td></tr>
					<tr><td colspan="3"><h2>RL 1.1 Data Dasar Rumah Sakit</h2></td></tr>
				</table>
				
				
				<table cellspacing="1" cellpadding="1" class="tb" width="95%">
			  <tr><td>1.</td><td>Nomor Kode RS</td><td>: <?php echo $data['nomor_kode_rs'];?></td></tr>
			  <tr><td>2.</td><td>Tanggal Registrasi</td><td>: <?php echo $data['tanggal_registrasi'];?></td></tr>
			  <tr><td>3.</td><td>Nama Rumah Sakit</td><td>: <?php echo $data['nama_rumah_sakit'];?></td></tr>
			  <tr><td>4.</td><td>Jenis Rumah Sakit</td><td>: <?php echo $data['jenis_rumah_sakit'];?></td></tr>
			  <tr><td>5.</td><td>Kelas Rumah Sakit</td><td>: <?php echo $data['kelas_rumah_sakit'];?></td></tr>
			  <tr><td>6.</td><td>Nama Direktur RS</td><td>: <?php echo $data['nama_direktur_rs'];?></td></tr>
			  <tr><td>7.</td><td>Nama Penyelenggara RS</td><td>: <?php echo $data['nama_penyelenggara_rs'];?></td></tr>
			  <tr><td>8.</td><td>Alamat / Lokasi RS</td><td></td></tr>
			  <tr><td></td><td>8.1 Kab / Kota</td><td>: <?php echo $data['kab_kota'];?></td></tr>
			  <tr><td></td><td>8.2 Kode Pos</td><td>: <?php echo $data['kode_pos'];?></td></tr>
			  <tr><td></td><td>8.3 Telepon</td><td>: <?php echo $data['telepon'];?></td></tr>
			  <tr><td></td><td>8.4 Fax</td><td>: <?php echo $data['fax'];?></td></tr>
			  <tr><td></td><td>8.5 Email</td><td>: <?php echo $data['email'];?></td></tr>
			  <tr><td></td><td>8.6 No Telp Bag. Umum / Humas RS</td><td>: <?php echo $data['nomor_telp_bag_umum'];?></td></tr>
			  <tr><td></td><td>8.7 Website</td><td>: <?php echo $data['website'];?></td></tr>
			  <tr><td>9.</td><td>Luas Rumah Sakit</td><td></td></tr>
			  <tr><td></td><td>9.1 Tanah</td><td>: <?php echo $data['tanah'];?></td></tr>
			  <tr><td></td><td>9.2 Bangunan</td><td>: <?php echo $data['bangunan'];?></td></tr>
			  <tr><td>10.</td><td>Surat Izin Penetapan</td><td></td></tr>
			  <tr><td></td><td>10.1 Nomor</td><td>: <?php echo $data['nomor'];?></td></tr>
			  <tr><td></td><td>10.2 Tanggal</td><td>: <?php echo $data['tanggal'];?></td></tr>
			  <tr><td></td><td>10.3 Oleh</td><td>: <?php echo $data['oleh'];?></td></tr>
			  <tr><td></td><td>10.4 Sifat</td><td>: <?php echo $data['sifat'];?></td></tr>
			  <tr><td></td><td>10.5 Masa Berlaku s/d thn</td><td>: <?php echo $data['masa_berlaku_thn'];?></td></tr>
			  <tr><td>11.</td><td>Status Penyelnggara Swasta</td><td>: <?php echo $data['status_penyelenggara_swasta'];?></td></tr>
			  <tr><td>12.</td><td>Akreditasi RS</td><td>: <?php echo $data['akreditasi_rs'];?></td></tr>
			  <tr><td></td><td>12.1 Pentahapan</td><td>: <?php echo $data['pentahapan'];?></td></tr>
			  <tr><td></td><td>12.2 Status</td><td>: <?php echo $data['status'];?></td></tr>
			  <tr><td></td><td>12.3 Tanggal Akreditasi</td><td>: <?php echo $data['tanggal_akreditasi'];?></td></tr>
			  <tr><td>13.</td><td>Tempat Tidur</td><td></td></tr>
			  <tr><td></td><td>13.1 VVIP</td><td>: <?php echo $data['vvip'];?></td></tr>
			  <tr><td></td><td>13.2 VIP</td><td>: <?php echo $data['vip'];?></td></tr>
			  <tr><td></td><td>13.3 I</td><td>: <?php echo $data['i'];?></td></tr>
			  <tr><td></td><td>13.4 II</td><td>: <?php echo $data['ii'];?></td></tr>
			  <tr><td></td><td>13.5 III</td><td>: <?php echo $data['iii'];?></td></tr>
			  <tr><td>14.</td><td>Tenaga Medis</td><td></td></tr>
			  <tr><td></td><td>14.1 Dokter Sp.A</td><td>: <?php echo $data['dokter_spa'];?></td></tr>
			  <tr><td></td><td>14.1 Dokter Sp.OG</td><td>: <?php echo $data['dokter_spog'];?></td></tr>
			  <tr><td></td><td>14.1 Dokter Sp.Pd</td><td>: <?php echo $data['dokter_sppd'];?></td></tr>
			  <tr><td></td><td>14.1 Dokter Sp.B</td><td>: <?php echo $data['dokter_spb'];?></td></tr>
			  <tr><td></td><td>14.1 Dokter Sp.Rad</td><td>: <?php echo $data['dokter_sprad'];?></td></tr>
			  <tr><td></td><td>14.1 Dokter Sp.RM</td><td>: <?php echo $data['dokter_sprm'];?></td></tr>
			  <tr><td></td><td>14.1 Dokter Sp.An</td><td>: <?php echo $data['dokter_span'];?></td></tr>
			  <tr><td></td><td>14.1 Dokter Sp.Jp</td><td>: <?php echo $data['dokter_spjp'];?></td></tr>
			  <tr><td></td><td>14.1 Dokter Sp.M</td><td>: <?php echo $data['dokter_spm'];?></td></tr>
			  <tr><td></td><td>14.1 Dokter Sp.THT</td><td>: <?php echo $data['dokter_sptht'];?></td></tr>
			  <tr><td></td><td>14.1 Dokter Sp.Kj</td><td>: <?php echo $data['dokter_spkj'];?></td></tr>
			  <tr><td></td><td>14.1 Dokter Umum</td><td>: <?php echo $data['dokter_umum'];?></td></tr>
			  <tr><td></td><td>14.1 Dokter Gigi</td><td>: <?php echo $data['dokter_gigi'];?></td></tr>
			  <tr><td></td><td>14.1 Dokter Gigi Spesialis</td><td>: <?php echo $data['dokter_gigi_spesialis'];?></td></tr>
			  <tr><td></td><td>14.1 Perawat</td><td>: <?php echo $data['perawat'];?></td></tr>
			  <tr><td></td><td>14.1 Bidan</td><td>: <?php echo $data['bidan'];?></td></tr>
			  <tr><td></td><td>14.1 Farmasi</td><td>: <?php echo $data['farmasi'];?></td></tr>
			  <tr><td></td><td>14.1 Tenaga Kesehatan Lainnya</td><td>: <?php echo $data['tenaga_kesehatan_lainnya'];?></td></tr>
			  <tr><td>15.</td><td>Tenaga Non Kesehatan</td><td>: <?php echo $data['tenaga_non_kesehatan'];?></td></tr>
			</table>
			</td>
		</tr>
	</table>
    </div>
	</div>
</div>
