<?php 

$tgl_kunjungan = "";
if(!empty($_GET['tahun'])){
	
	$tahun	= isset($_REQUEST['tahun']) ? $_REQUEST['tahun'] : date('Y');
	$bulan	= isset($_REQUEST['bulan']) ? $_REQUEST['bulan'] : date('m');
}else{
	$tahun	= date('Y');
	$bulan	= date('m');
}

?>


<div align="center">
    <div id="frame" style="width:95%">
    <div id="frame_title">
			
		<table cellpadding="0" class="tb" width="95%" cellspacing="0">
			<tr><td rowspan="2" style="width:110px;"><img src="<?php echo _BASE_;?>/img/logobaktihusda.gif"></td><td><h2>Formulir 5.2</h2></td><td rowspan="2"><div style="border:1px dashed #999; padding:10px; display:block; font-style:italic; width:170px;">Ditjen Bina Upaya Kesehatan <br />Kementrian Kesehatan RI</div> 
</td></tr>
			<tr><td><h1>KUNJUNGAN RAWAT JALAN</h1></td></tr>
		</table>
			
			<?php
			$blnname	= array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
			?>
			<br><br>
			<table cellpadding="0" class="tb" width="95%" cellspacing="0">
				<tr><td> Kode RS </td><td>: <?php echo $kode_rs;?></td></tr>
                <tr><td> Nama RS </td><td>: <?php echo $nama_rs;?></td></tr>
                <tr><td> Tahun </td><td>: <?php echo $tahun;?></td></tr>
				<tr><td> Bulan </td><td>: <?php echo $blnname[$bulan - 1];?></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
			</table>
			
			<table cellspacing="1" cellpadding="1" class="tb" width="95%">
			<thead>
				<tr><th>NO</th><th>NAMA KEGIATAN</th><th>JUMLAH</th></tr>
				<tr><td width="20px">1</td><td width="220px">2</td><td>3</td></tr>
			</thead>
			<tbody>
				<?php
				$sql	= mysql_query('SELECT a.nama_unit, COUNT(b.idxdaftar) AS jumlah
FROM m_unit a
LEFT JOIN t_pendaftaran b ON a.kode_unit = b.KDPOLY
WHERE kode_unit <> 0 AND kode_unit <> 14 AND kode_unit <> 32
AND pendapatan_unit = "Rawat Jalan" and YEAR(b.tglreg) = '.$tahun.' AND MONTH(b.tglreg) = '.$bulan.'
GROUP BY a.kode_unit');
				if(mysql_num_rows($sql) > 0)
				{
					$i	= 1;
					while($data	= mysql_fetch_array($sql))
					{
						echo '<tr><td align="center">'.$i.'</td><td>'.$data['nama_unit'].'</td><td align="right">'.$data['jumlah'].'</td></tr>';
						$i++;
					}
				}
				?>
				
			</tbody>
			</table>
    </div>
	</div>
</div>
