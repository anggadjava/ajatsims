<?php 

$tgl_kunjungan = "";
if(!empty($_GET['tahun'])){
	
	$tahun	= isset($_REQUEST['tahun']) ? $_REQUEST['tahun'] : date('Y');
	$bulan	= isset($_REQUEST['bulan']) ? $_REQUEST['bulan'] : date('m');
}else{
	$tahun	= date('Y');
	$bulan	= date('m');
}

?>


<div align="center">
    <div id="frame" style="width:95%">
    <div id="frame_title">
			
		<table cellpadding="0" class="tb" width="95%" cellspacing="0">
			<tr><td rowspan="2" style="width:110px;"><img src="<?php echo _BASE_;?>/img/logobaktihusda.gif"></td><td><h2>Formulir 5.1</h2></td><td rowspan="2"><div style="border:1px dashed #999; padding:10px; display:block; font-style:italic; width:170px;">Ditjen Bina Upaya Kesehatan <br />Kementrian Kesehatan RI</div> 
</td></tr>
			<tr><td><h1>PENGUNJUNG</h1></td></tr>
		</table>
			
			<?php
			$blnname	= array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
			?>
			<br><br>
			<table cellpadding="0" class="tb" width="95%" cellspacing="0">
				<tr><td> Kode RS </td><td>: <?php echo $kode_rs;?></td></tr>
                <tr><td> Nama RS </td><td>: <?php echo $nama_rs;?></td></tr>
                <tr><td> Tahun </td><td>: <?php echo $tahun;?></td></tr>
				<tr><td> Bulan </td><td>: <?php echo $blnname[$bulan -1];?></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
			</table>
			
			<table cellspacing="1" cellpadding="1" class="tb" width="95%">
			<thead>
				<tr><th>NO</th><th>NAMA KEGIATAN</th><th>JUMLAH</th></tr>
				<tr><td>1</td><td>2</td><td>3</td></tr>
			</thead>
			<tbody>
				<?php
				$sql	= mysql_query('SELECT SUM(IF(pasienbaru = 1,1,0)) AS pasienbaru,  SUM(IF(pasienbaru <> 1,1,0)) AS pasienlama
FROM t_pendaftaran WHERE YEAR(tglreg) = '.$tahun.' AND MONTH(tglreg) = '.$bulan);
$row	= mysql_fetch_array($sql);
				?>
				<tr align="center"><td width="20px">1</td><td width="220px">Pengunjung Baru</td><td align="right"><?php echo $row['pasienbaru'];?></td></tr>
				<tr align="center"><td width="20px">2</td><td width="220px">Pengunjung Lama</td><td align="right"><?php echo $row['pasienlama'];?></td></tr>
			</tbody>
			</table>
    </div>
	</div>
</div>
